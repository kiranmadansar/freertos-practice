/********************************************************************************************************
*
* UNIVERSITY OF COLORADO BOULDER
*
* @file Q2.c
* @brief Blink Two LEDs at two different frequencies
*
* Two tasks are created and each task creates a timer which blinks
* LED at 2 Hz and 4 Hz
*
* @author Kiran Hegde
* @date  4/8/2018
* @tools Code Composer Studio
*
********************************************************************************************************/

/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "timers.h"
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/debug.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"

TimerHandle_t timer1, timer2;
//*****************************************************************************
//
// This hook is called by FreeRTOS when an stack overflow error is detected.
//
//*****************************************************************************
void
vApplicationStackOverflowHook(xTaskHandle *pxTask, char *pcTaskName)
{
    //
    // This function can not return, so loop forever.  Interrupts are disabled
    // on entry to this function, so no processor interrupts will interrupt
    // this loop.
    //
    while(1)
    {
    }
}

/* Callback function for timer 2*/
void TimerCallback2(TimerHandle_t timer2)
{
    if(MAP_GPIOPinRead(GPIO_PORTN_BASE, GPIO_PIN_0))
        MAP_GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 0);
    else
        MAP_GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 1);
}

/* TASK1 */
static void Task1(void *pvParameters)
{
    timer2 = xTimerCreate("Timer2", pdMS_TO_TICKS(500), pdTRUE, 0, TimerCallback2 );
    if(timer2==NULL)
       {
           printf("timer not created\n");
       }
       else
       {
           xTimerStart(timer2, 0);
       }
    for(;;)
    {
        ;
    }
}

/* Callback function for timer1 */
void TimerCallback1(TimerHandle_t timer1)
{
    if(MAP_GPIOPinRead(GPIO_PORTN_BASE, GPIO_PIN_1))
        MAP_GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_1, 0);
    else
        MAP_GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_1, 1);
}

/* TASK2 */
static void Task2(void *pvParameters)
{
    timer1 = xTimerCreate("Timer1", pdMS_TO_TICKS(250), pdTRUE, 0, TimerCallback1 );
    if(timer1==NULL)
    {
        printf("timer not created\n");
    }
    else
    {
        xTimerStart(timer1, 0);
    }
    for(;;)
    {
        ;
    }
}

/* Initialization function for task1 */
uint32_t Task1Init(void)
{

    if(xTaskCreate(Task1,(const portCHAR *)"Task1",configMINIMAL_STACK_SIZE,NULL,2,NULL)!=pdTRUE)
    {
        return (1);
    }
    return (0);
}

/* Initialization function for task2 */
uint32_t Task2Init(void)
{

    if(xTaskCreate(Task2,(const portCHAR *)"Task2",configMINIMAL_STACK_SIZE,NULL,1,NULL)!=pdTRUE)
    {
        return (1);
    }
    return (0);
}

int main()
{
    MAP_SysCtlClockFreqSet((SYSCTL_CFG_VCO_480 | SYSCTL_USE_PLL | SYSCTL_XTAL_25MHZ | SYSCTL_OSC_MAIN), 120000000U);

    //
    // Enable the GPIO port that is used for the on-board LED.
    //
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);
    //IntMasterEnable();
    //
    // Enable the GPIO pins for the LED (PN0).
    //
    MAP_GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE, GPIO_PIN_1);
    MAP_GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE, GPIO_PIN_0);
    MAP_GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_1, 1);
    MAP_GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 1);

    /* Create Task1 */
    if(Task1Init() != 0)
    {
        while(1)
        {
        }
    }

    /* Create Task2 */
    if(Task2Init() != 0)
    {
        while(1)
        {
        }
    }

    /* Schedule tasks */
    vTaskStartScheduler();

    while(1)
    {
    }
    return 0;
}

