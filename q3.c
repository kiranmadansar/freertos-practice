/********************************************************************************************************
*
* UNIVERSITY OF COLORADO BOULDER
*
* @file Q3.c
* @brief Task notify and queue in FreeRTOS
*
* Create three task and use xTaskNotify to synchronize the tasks. Use Queue for
* communication
*
* @author Kiran Hegde
* @date  4/7/2018
* @tools Code Composer Studio
*
********************************************************************************************************/

/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/debug.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"
#include "main.h"
#include "drivers/pinout.h"

/********************************************************************************************************
*
* Global Variables
*
********************************************************************************************************/
TimerHandle_t timer1, timer2;
TaskHandle_t taskNotify1;
QueueHandle_t queue;
//*****************************************************************************
//
// This hook is called by FreeRTOS when an stack overflow error is detected.
//
//*****************************************************************************
void
vApplicationStackOverflowHook(xTaskHandle *pxTask, char *pcTaskName)
{
    //
    // This function can not return, so loop forever.  Interrupts are disabled
    // on entry to this function, so no processor interrupts will interrupt
    // this loop.
    //
    while(1)
    {
    }
}

/* Call back function for timer2 */
void TimerCallback2(TimerHandle_t timer2)
{
    TickType_t cTick = xTaskGetTickCount();
    xQueueSend(queue, &cTick, 10);
    xTaskNotify(taskNotify1, 0x02, eSetBits);
}

/* TASK2*/
static void Task2(void *pvParameters)
{
    timer2 = xTimerCreate("Timer2", pdMS_TO_TICKS(250), pdTRUE, 0, TimerCallback2 );
    if(timer2==NULL)
       {
           UARTprintf("timer not created\n");
       }
       else
       {
           xTimerStart(timer2, 0);
       }
    for(;;)
    {
        ;
    }
}

/*Callback function for timer1*/
void TimerCallback1(TimerHandle_t timer1)
{
    xTaskNotify(taskNotify1, 0x01, eSetBits);
}

/* TASK1 */
static void Task1(void *pvParameters)
{
    timer1 = xTimerCreate("Timer1", pdMS_TO_TICKS(500), pdTRUE, 0, TimerCallback1 );
    if(timer1==NULL)
    {
        UARTprintf("timer not created\n");
    }
    else
    {
        xTimerStart(timer1, 0);
    }
    for(;;)
    {
        ;
    }
}

/* TASK3 blinks LED and prints count to UART*/
static void Task3(void *pvParameters)
{
    uint32_t temp;
    TickType_t count;
    for(;;)
    {
        xTaskNotifyWait(0x00, 0xFFFFFFFF, &temp, portMAX_DELAY);
        if((temp & 0x01) != 0)
        {
            if(MAP_GPIOPinRead(GPIO_PORTN_BASE, GPIO_PIN_0))
                MAP_GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 0);
            else
                MAP_GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 1);
        }
        if((temp & 0x02)!=0)
        {
            xQueueReceive(queue, &count, 10);
            UARTprintf("Tick count : %d\n", count);
        }
    }
}

/* Initialization function for task1 */
uint32_t Task1Init(void)
{
    if(xTaskCreate(Task1,(const portCHAR *)"Task1",configMINIMAL_STACK_SIZE,NULL,1,NULL)!=pdTRUE)
    {
        return (1);
    }
    return (0);
}

/* Initialization function for task2 */
uint32_t Task2Init(void)
{
    if(xTaskCreate(Task2,(const portCHAR *)"Task2",configMINIMAL_STACK_SIZE,NULL,1,NULL)!=pdTRUE)
    {
        return (1);
    }
    return (0);
}

/* Initialization function for task3 */
uint32_t Task3Init(void){

    if(xTaskCreate(Task3,(const portCHAR *)"Task3",configMINIMAL_STACK_SIZE,NULL,1,&taskNotify1)!=pdTRUE){
        return (1);
    }
    return (0);
}

int main()
{
    uint32_t g_ui32SysClock = ROM_SysCtlClockFreqSet(
            (SYSCTL_XTAL_25MHZ | SYSCTL_OSC_MAIN |
             SYSCTL_USE_PLL | SYSCTL_CFG_VCO_480),
             SYSTEM_CLOCK);
    //
    // Enable the GPIO port that is used for the on-board LED.
    //
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);
    //IntMasterEnable();
    /* Open QUEUE */
    queue = xQueueCreate(100, sizeof(TickType_t));
    if(queue==NULL)
    {
        printf("Queue creation failed\n");
    }

    PinoutSet(false, false);
    /* configure UART */
    UARTStdioConfig(0, 115200, SYSTEM_CLOCK);

    //
    // Enable the GPIO pins for the LED (PN0).
    //
    MAP_GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE, GPIO_PIN_1);
    MAP_GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE, GPIO_PIN_0);

    MAP_GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_1, 1);
    MAP_GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 1);

    /* Create Task1 */
    if(Task1Init() != 0)
    {
        while(1)
        {
        }
    }

    /* Create Task2 */
    if(Task2Init() != 0)
    {
        while(1)
        {
        }
    }

    /* Create Task3 */
    if(Task3Init() != 0)
    {
        while(1)
        {
        }
    }

    /* SCHEDULE THE TASK */
    vTaskStartScheduler();

    while(1)
    {
    }
    return 0;
}

void __error__(char *pcFilename, uint32_t ui32Line)
{
    // Place a breakpoint here to capture errors until logging routine is finished
    while (1)
    {
    }
}

